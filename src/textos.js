
export const TEXTOS = {
    sform: ["Formulari de subscripció", "Formulario de suscripción", "Subscription form"],
    scompany: ["Empresa", "Empresa", "Company's name"],
    spassword: ["Contrasenya","Contraseña", "Password"],
    saddress: ["Adreça", "Dirección", "Adress"],
    sphone: ["Número de telèfon", "Número de teléfono", "Phone Number"],
    scity: ["Ciutat", "Ciudad", "City"],
    sstate: ["Província", "Provincia", "Province"],
    szip: ["Codi postal", "Código Postal", "Zip"],
    sselect: ["Selecciona el tipus", "Selecciona el tipo", "Select type"],
    ssubmit: ["Enviar", "Enviar", "Submit"],
    scontact: ["Contacta'ns","Contáctenos", "Contact Us"],
    sname: ["Nom","Nombre", "Name"],
    scomment: ["Comentaris", "Comentarios", "Comments"],
    sinfo: ["Els nostres mitjans de comunicació social, correu electrònic i altres coses que voleu afegir", "Nuestras redes sociales, correo electrónico y otras cosas que desea agregar", "Our Social Medias, E-mail and other stuff that you want to add"],
    sloading: ["Carregant dades...", "Cargando datos...", "Loading..."],
    sreturn:["Tornar","Volver","Return"],
    smap:["Mapa", "Mapa", "Map"],
    semail:["Aquest correu electrònic ja està registrat al nostre sistema", "Este correo electrónico ya está registrado en nuestro sistema", "This email is already registered in our system"]
}




